package com.muni.pv239.foosballstats.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muni.pv239.foosballstats.R
import com.muni.pv239.foosballstats.utils.inflate

class PlayerRecyclerAdapter(private val itemList: MutableList<String>) :
    RecyclerView.Adapter<PlayersItemHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayersItemHolder {
        val inflatedView = parent.inflate(R.layout.profile_item, false)
        return PlayersItemHolder(inflatedView)
    }

    override fun getItemCount(): Int = itemList.size


    override fun onBindViewHolder(holder: PlayersItemHolder, position: Int) {
        val item = itemList[position]
        holder.bind(item)
    }
}