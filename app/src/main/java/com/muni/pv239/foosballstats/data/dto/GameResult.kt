package com.muni.pv239.foosballstats.data.dto

enum class GameResult(val value: Int) {
    LOSS(0),
    WIN(1),
    DRAW(2);

    companion object {
        fun reverseResult(gameResult: GameResult): GameResult {
            return when (gameResult) {
                WIN -> LOSS
                LOSS -> WIN
                else -> DRAW
            }
        }
    }
}