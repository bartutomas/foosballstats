package com.muni.pv239.foosballstats

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.muni.pv239.foosballstats.data.repository.RecyclerRepository
import com.muni.pv239.foosballstats.ui.adapter.GameRecyclerAdapter
import kotlinx.android.synthetic.main.activity_game.*


const val EXTRA_MESSAGE = "date"
class GameActivity : AppCompatActivity() {
    private lateinit var adapter: GameRecyclerAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var repository: RecyclerRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        repository = RecyclerRepository(this)
        // Get the Intent that started this activity and extract the string
        val message = intent.getStringExtra(EXTRA_MESSAGE)
        title = message
        val itemList = repository.getGamesForDate(message?.toString()).toMutableList()
        adapter = GameRecyclerAdapter(itemList)
        linearLayoutManager = LinearLayoutManager(this)
        game_recycler.layoutManager = linearLayoutManager
        game_recycler.adapter = adapter

        add_new_game.setOnClickListener {
            val intent = Intent(this, AddGameActivity::class.java)
            this.startActivity(intent)
        }
    }
}
