package com.muni.pv239.foosballstats.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muni.pv239.foosballstats.R
import com.muni.pv239.foosballstats.model.Game
import com.muni.pv239.foosballstats.utils.inflate

class GameRecyclerAdapter(private val itemList: MutableList<Game>) :
    RecyclerView.Adapter<GameItemHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameItemHolder {
        val inflatedView = parent.inflate(R.layout.game_recycler_item, false)
        return GameItemHolder(inflatedView)
    }

    override fun getItemCount(): Int = itemList.size


    override fun onBindViewHolder(holder: GameItemHolder, position: Int) {
        val item = itemList[position]
        holder.bind(item)
    }
}