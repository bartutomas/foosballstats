package com.muni.pv239.foosballstats.data.database.dao

import androidx.room.*
import com.muni.pv239.foosballstats.model.Game
import com.muni.pv239.foosballstats.model.Games
import com.muni.pv239.foosballstats.model.Player

@Dao
interface RecyclerDao {
    @Query("SELECT * from game")
    fun getAllItem(): List<Game>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: Game)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPlayer(item: Player)

    @Query("DELETE FROM game")
    fun deleteAll()

    @Query("DELETE FROM player WHERE playerId=:playerId")
    fun deletePlayer(playerId: String?)

    @Query("SELECT date, sum(result = 1) as wins, sum(result = 0) as loses, sum(result = 2) as draws FROM game GROUP BY date")
    fun getGames(): List<Games>

    @Query("SELECT * FROM game WHERE date=:date")
    fun getGamesForDate(date: String?): List<Game>

    @Query("SELECT * FROM game WHERE gameId=:gameId")
    fun getGameById(gameId: Int?): Game

    @Query("SELECT * FROM player WHERE playerId=:name")
    fun getPlayer(name: String?): Player?

    @Query("SELECT playerId from player")
    fun getAllPlayers(): List<String>

    @Update
    fun updatePlayer(player: Player)

    @Query("SELECT count(*) FROM game WHERE attack=:playerName OR attack_opp=:playerName OR offend=:playerName OR offend_opp=:playerName ")
    fun getGamesCountPlayedByPlayer(playerName: String): Long

}