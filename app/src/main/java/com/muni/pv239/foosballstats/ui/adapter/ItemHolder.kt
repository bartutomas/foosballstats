package com.muni.pv239.foosballstats.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.muni.pv239.foosballstats.GameActivity
import com.muni.pv239.foosballstats.model.Games
import kotlinx.android.synthetic.main.recycler_item.view.*

const val EXTRA_MESSAGE = "date"
private var context: Context? = null

class ItemHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
    private var view: View = v

    init {
        v.setOnClickListener(this)
        context = v.context
    }


    @SuppressLint("SetTextI18n")
    fun bind(item: Games) {
        view.recycler_item_date.text = item.date
        view.recycler_item_wins.text = item.wins.toString()+"W"
        view.recycler_item_draws.text = item.draws.toString()+"D"
        view.recycler_item_loses.text = item.loses.toString()+"L"
    }

    override fun onClick(v: View) {
        Log.d("RecyclerView", "CLICK! on"+view.recycler_item_date.text)
        val intent = Intent(context, GameActivity::class.java)
        intent.putExtra(EXTRA_MESSAGE, view.recycler_item_date.text)
        context?.startActivity(intent)
    }


}