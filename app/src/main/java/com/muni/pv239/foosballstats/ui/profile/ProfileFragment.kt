package com.muni.pv239.foosballstats.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.muni.pv239.foosballstats.AddPlayerActivity
import com.muni.pv239.foosballstats.data.repository.RecyclerRepository
import com.muni.pv239.foosballstats.databinding.FragmentProfileBinding
import com.muni.pv239.foosballstats.ui.adapter.PlayerRecyclerAdapter

class ProfileFragment : Fragment() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: PlayerRecyclerAdapter
    private lateinit var binding: FragmentProfileBinding
    private lateinit var repository: RecyclerRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context?.let {
            repository = RecyclerRepository(it)
        }
    }

    override fun onResume() {  // After a pause OR at startup
        super.onResume()
        val itemList = repository.getAllPlayers().toMutableList()
        adapter = PlayerRecyclerAdapter(itemList)
        binding.profileRecycler.adapter = adapter
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(layoutInflater, container, false)

        linearLayoutManager = LinearLayoutManager(context)
        binding.profileRecycler.layoutManager = linearLayoutManager

        binding.addNew.setOnClickListener {
            val intent = Intent(context, AddPlayerActivity::class.java)
            context?.startActivity(intent)
        }
        val itemList = repository.getAllPlayers().toMutableList()
        adapter = PlayerRecyclerAdapter(itemList)
        binding.profileRecycler.adapter = adapter
        return binding.root
    }
}
