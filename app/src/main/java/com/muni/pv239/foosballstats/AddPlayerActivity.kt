package com.muni.pv239.foosballstats

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.util.Base64
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.muni.pv239.foosballstats.data.repository.RecyclerRepository
import com.muni.pv239.foosballstats.model.Player
import kotlinx.android.synthetic.main.activity_add_player.*
import java.io.ByteArrayOutputStream


class AddPlayerActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private lateinit var repository: RecyclerRepository
    private val REQUEST_IMAGE_CAPTURE = 1
    var imageBitmap: Bitmap? = null
    private var message: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_player)
        repository = RecyclerRepository(this)
        message = intent.getStringExtra("player")

        if (message != null) {
            prepareDetailView(message!!)
        } else {
            title = getString(R.string.add_new_player)
            this.togglePlayerStatsVisibility(View.GONE)
        }
        button_photo.setOnClickListener {
            dispatchTakePictureIntent()
        }
    }

    private fun prepareDetailView(message: String) {
        val player = repository.getPlayer(message)!!
        title = getString(R.string.player_detail)
        text_nick.text = Editable.Factory.getInstance().newEditable(message)
        text_nick.isEnabled = false
        text_descr.text = Editable.Factory.getInstance().newEditable(player.description)
        this.togglePlayerStatsVisibility(View.VISIBLE)
        gamesWon?.text = player.gamesWon.toString()
        gamesLost?.text = player.gamesLost.toString()
        decodeBase64ToBitmap(player.profilePic)
    }

    private fun togglePlayerStatsVisibility(visibility: Int) {
        gamesWon?.visibility = visibility
        gamesLost?.visibility = visibility
        gamesWonLabel?.visibility = visibility
        gamesLostLabel?.visibility = visibility
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (!isNewPlayer()) {
            menuInflater.inflate(R.menu.delete_button_menu, menu)
        }
        menuInflater.inflate(R.menu.save_button_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.button_save_menu -> {
                if (!validatePlayer()) {
                    showNotification(getString(R.string.player_error_missing_attributes))
                } else {
                    savePlayer()
                    finish()
                }
            }
            R.id.button_delete_menu -> {
                deletePlayer()
            }
            else -> {
                onBackPressed()
            }
        }
        return true
    }

    private fun validatePlayer(): Boolean {
        return !text_nick.text.isNullOrEmpty()
    }

    private fun savePlayer() {
        val player = if (isNewPlayer()) createNewPlayer() else editPlayer()
        if (player == null) {
            showNotification(getString(R.string.player_name_already_exists))
            return
        }
        if (isNewPlayer()) {
            repository.addNewPlayer(player)
            showNotification(getString(R.string.player_added))
        } else {
            repository.updatePlayer(player)
            showNotification(getString(R.string.player_edited))
        }
    }

    private fun isNewPlayer() = message == null

    private fun createNewPlayer(): Player? {
        val playerName = text_nick.text.toString()
        if (isNewPlayer() && isPlayerWithSameNameExists(playerName)) {
            return null
        }

        return Player(
            playerName,
            text_descr.text.toString(),
            "0",
            encodeBitmapToBase64(),
            0,
            0
        )
    }

    private fun editPlayer(): Player {
        val player = repository.getPlayer(message)!!
        return player.copy(description = text_descr.text.toString(), profilePic = encodeBitmapToBase64())
    }

    private fun isPlayerWithSameNameExists(playerName: String): Boolean {
        return repository.getPlayer(playerName) != null
    }

    private fun deletePlayer() {
        val gamesCountPlayedByPlayer =
            repository.getGamesCountPlayedByPlayer(text_nick.text.toString())

        if (gamesCountPlayedByPlayer > 0L) {
            showDeletePlayerConfirmDialog()
            return
        }

        repository.deletePlayer(message)
        finish()
    }

    private fun showDeletePlayerConfirmDialog() {
        val builder = AlertDialog.Builder(this)
            .setTitle(getString(R.string.delete_player))
            .setMessage(getText(R.string.delete_player_warning))
            .setPositiveButton(getString(R.string.yes)) { dialog, _ ->
                repository.deletePlayer(message)
                dialog.cancel()
                finish()
            }.setNegativeButton(getString(R.string.no)) { dialog, _ ->
                dialog.cancel()
            }
        builder.show()
    }

    private fun showNotification(text: String) {
        Toast.makeText(baseContext, text, Toast.LENGTH_SHORT).show()
    }

    private fun decodeBase64ToBitmap(strBase64: String) {
        if (strBase64 == "") {
            return
        }
        val decodedString: ByteArray =
            Base64.decode(strBase64, Base64.DEFAULT)
        val decodedByte =
            BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        imageView.setImageBitmap(decodedByte)
    }

    private fun encodeBitmapToBase64(): String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        imageBitmap?.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream) ?: return ""
        val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            imageBitmap = data?.extras?.get("data") as Bitmap?
            imageView.setImageBitmap(imageBitmap)
        }
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {}

    override fun onNothingSelected(arg0: AdapterView<*>) {}

}
