package com.muni.pv239.foosballstats.model

data class Games(val date: String, val wins: Int, val loses: Int, val draws: Int)