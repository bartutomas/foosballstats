package com.muni.pv239.foosballstats.ui.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.muni.pv239.foosballstats.AddGameActivity
import com.muni.pv239.foosballstats.model.Game
import kotlinx.android.synthetic.main.game_recycler_item.view.*

private var context: Context? = null
class GameItemHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
    private var view: View = v
    private var gameId: Int = -1

    init {
        v.setOnClickListener(this)
        context = v.context
    }


    fun bind(item: Game) {
        view.game_recycler_item_player1.text = item.attack
        gameId = item.gameId
        view.game_recycler_item_player2.text = item.offed
        view.game_recycler_item_player3.text = item.attack_opp
        view.game_recycler_item_player4.text = item.offend_opp
        view.game_recycler_item_scored.text = item.goalsScored.toString()
        view.game_recycler_item_received.text = item.goalsReceived.toString()
    }

    override fun onClick(v: View) {
        Log.d("RecyclerView", "CLICK! on"+view.game_recycler_item_player1.toString())
        val intent = Intent(context, AddGameActivity::class.java)
        intent.putExtra("game", gameId)
        context?.startActivity(intent)
    }


}