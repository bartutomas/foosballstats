package com.muni.pv239.foosballstats.data.repository

import android.content.Context
import com.muni.pv239.foosballstats.data.database.db.MyDatabase
import com.muni.pv239.foosballstats.data.dto.GameResult
import com.muni.pv239.foosballstats.data.dto.NewGameDTO
import com.muni.pv239.foosballstats.model.Game
import com.muni.pv239.foosballstats.model.Games
import com.muni.pv239.foosballstats.model.Player

class RecyclerRepository(context: Context) {
    val recyclerDao = MyDatabase.getDatabase(context).recyclerDao()
    fun addNewItem(item: Game){
        recyclerDao.insert(item)
    }

    fun addNewGame(newGameDTO: NewGameDTO) {
        // at first we have to update or create players because game is dependant on them
        createOrUpdatePlayer(newGameDTO.team1Attacker, newGameDTO.team1Result)
        createOrUpdatePlayer(newGameDTO.team1Defender, newGameDTO.team1Result)
        createOrUpdatePlayer(newGameDTO.team2Attacker, GameResult.reverseResult(newGameDTO.team1Result))
        createOrUpdatePlayer(newGameDTO.team2Defender, GameResult.reverseResult(newGameDTO.team1Result))

        recyclerDao.insert(newGameDTO.game)
    }

    private fun createOrUpdatePlayer(playerName: String, gameResult: GameResult) {
        val player = recyclerDao.getPlayer(playerName)
        if (player == null) {
            val stubPlayer = Player.stubPlayer(playerName, gameResult)
            addNewPlayer(stubPlayer)
        } else {
            val playerToUpdate = when (gameResult) {
                GameResult.WIN -> player.copy(gamesWon = player.gamesWon + 1)
                GameResult.LOSS -> player.copy(gamesLost = player.gamesLost + 1)
                else -> player
            }
            recyclerDao.updatePlayer(playerToUpdate)
        }
    }

    fun addNewPlayer(item: Player){
        recyclerDao.insertPlayer(item)
    }

    fun deletePlayer(id: String?){
        recyclerDao.deletePlayer(id)
    }

    fun getAllGames(): List<Games>{
        return recyclerDao.getGames()
    }

    fun getAllPlayers(): List<String>{
        return recyclerDao.getAllPlayers()
    }

    fun getPlayer(name: String?): Player? {
        return recyclerDao.getPlayer(name)
    }
    fun getGamesForDate(date: String?): List<Game> {
        return recyclerDao.getGamesForDate(date)
    }
    fun getGameById(gameId: Int?): Game {
        return recyclerDao.getGameById(gameId)
    }
    fun getGamesCountPlayedByPlayer(playerName: String): Long {
        return recyclerDao.getGamesCountPlayedByPlayer(playerName)
    }
    fun updatePlayer(player: Player) {
        return recyclerDao.updatePlayer(player)
    }
}