package com.muni.pv239.foosballstats.data.database.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.muni.pv239.foosballstats.data.database.dao.RecyclerDao
import com.muni.pv239.foosballstats.model.Game
import com.muni.pv239.foosballstats.model.Player


@Database(
    entities = [Player::class, Game::class],
    version = 1,
    exportSchema = false)

abstract class MyDatabase : RoomDatabase(){
    abstract fun recyclerDao(): RecyclerDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: MyDatabase? = null

        const val MY_DATABASE = "foosball_stat"

        fun getDatabase(context: Context): MyDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

                val instance = Room.databaseBuilder(
                    context,
                    MyDatabase::class.java,
                    MY_DATABASE
                )
                    .allowMainThreadQueries().build()
                INSTANCE = instance
                return instance

        }
    }
}