package com.muni.pv239.foosballstats.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.muni.pv239.foosballstats.data.dto.GameResult
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "player")
data class Player(@PrimaryKey(autoGenerate = false) val playerId: String,
                  @ColumnInfo val description: String,
                  @ColumnInfo val myRating: String,
                  @ColumnInfo val profilePic: String,
                  @ColumnInfo val gamesWon: Int,
                  @ColumnInfo val gamesLost: Int): Parcelable {
    companion object {
        fun stubPlayer(
            playerId: String,
            gameResult: GameResult
        ): Player {
            val gamesWonCount = if (gameResult == GameResult.WIN) 1 else 0
            val gamesLostCount = if (gameResult == GameResult.LOSS) 1 else 0
            return Player(
                playerId, "", "0", "",
                gamesWonCount, gamesLostCount
            )
        }
    }
}

@Parcelize
@Entity(tableName = "game",
    foreignKeys = [ForeignKey(entity = Player::class,
        parentColumns = arrayOf("playerId"),
        childColumns = arrayOf("attack"),
        onDelete = ForeignKey.CASCADE), ForeignKey(entity = Player::class,
        parentColumns = arrayOf("playerId"),
        childColumns = arrayOf("offend"),
        onDelete = ForeignKey.CASCADE), ForeignKey(entity = Player::class,
        parentColumns = arrayOf("playerId"),
        childColumns = arrayOf("offend_opp"),
        onDelete = ForeignKey.CASCADE), ForeignKey(entity = Player::class,
        parentColumns = arrayOf("playerId"),
        childColumns = arrayOf("attack_opp"),
        onDelete = ForeignKey.CASCADE)]
)
data class Game(
    @PrimaryKey(autoGenerate = true)
    val gameId: Int,
    @ColumnInfo(name = "date")
    val date: String,
    @ColumnInfo(name = "goalsScored")
    val goalsScored: Int,
    @ColumnInfo(name = "goalsReceived")
    val goalsReceived: Int,
    @ColumnInfo(name = "place")
    val place: String,
    @ColumnInfo(name = "result")
    val result: Int,
    @ColumnInfo(name = "attack")
    val attack: String,
    @ColumnInfo(name = "offend")
    val offed: String,
    @ColumnInfo(name = "attack_opp")
    val attack_opp: String,
    @ColumnInfo(name = "offend_opp")
    val offend_opp: String
): Parcelable