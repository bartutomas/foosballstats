package com.muni.pv239.foosballstats

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.muni.pv239.foosballstats.data.dto.GameResult
import com.muni.pv239.foosballstats.data.dto.NewGameDTO
import com.muni.pv239.foosballstats.data.repository.RecyclerRepository
import com.muni.pv239.foosballstats.model.Game
import kotlinx.android.synthetic.main.activity_add_game.*
import java.text.DateFormat
import java.text.DateFormat.getDateInstance
import java.util.*


class AddGameActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private lateinit var repository: RecyclerRepository
    private var message: Int? = null

    override fun onResume() {
        super.onResume()
        repository = RecyclerRepository(this)
        val playerList = repository.getAllPlayers()
        val adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, playerList)
        // Set Adapter to Spinner
        team1_attacker_text_view.setAdapter(adapter)
        team1_defender_text_view.setAdapter(adapter)
        team2_attacker_text_view.setAdapter(adapter)
        team2_defender_text_view.setAdapter(adapter)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_game)
        repository = RecyclerRepository(this)
        val playerList = repository.getAllPlayers()
        val adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, playerList)
        // Set Adapter to Spinner
        team1_attacker_text_view.setAdapter(adapter)
        team1_defender_text_view.setAdapter(adapter)
        team2_attacker_text_view.setAdapter(adapter)
        team2_defender_text_view.setAdapter(adapter)
        message = intent.getIntExtra("game", -1)
        if (message != -1) {
            prepareDetail(message, playerList)
        } else {
            title = "Add new game"
            val dateFormat: DateFormat = getDateInstance()
            val date = Date()
            date_picker.setText(dateFormat.format(date).toString())
            setListeners()
        }
    }

    private fun setListeners() {
        date_picker.setOnTouchListener(View.OnTouchListener  { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                val calendar = Calendar.getInstance()
                val date = getDateInstance().parse(date_picker.text.toString())
                calendar.time = date!!

                val picker = DatePickerDialog(
                    this,
                    OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                        val calendar = Calendar.getInstance()
                        calendar.set(Calendar.YEAR, year)
                        calendar.set(Calendar.MONTH, monthOfYear)
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                        val sdf = getDateInstance()
                        date_picker.setText(sdf.format(calendar.time))
                    },
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
                )
                picker.show()
                date_picker.requestFocus()
                val inputMethodManager: InputMethodManager = this.getSystemService(
                    Activity.INPUT_METHOD_SERVICE
                ) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(
                    this.currentFocus!!.windowToken, 0
                )
            }
            return@OnTouchListener true
        })

        team1_attacker_text_view.setOnClickListener {
            team1_attacker_text_view.showDropDown()
        }
        team1_defender_text_view.setOnClickListener {
            team1_defender_text_view.showDropDown()
        }
        team2_attacker_text_view.setOnClickListener {
            team2_attacker_text_view.showDropDown()
        }
        team2_defender_text_view.setOnClickListener {
            team2_defender_text_view.showDropDown()
        }
    }

    private fun prepareDetail(message: Int?, playerList: List<String>) {
        val game = repository.getGameById(message)
        title = "Detail of the Game"
        team1_attacker_text_view.setText(game.attack, false)
        team1_attacker.isEnabled = false
        team2_attacker_text_view.setText(game.attack_opp, false)
        team2_attacker.isEnabled = false
        team1_defender_text_view.setText(game.offed, false)
        team1_defender.isEnabled = false
        team2_defender_text_view.setText(game.offend_opp, false)
        team2_defender.isEnabled = false
        edit_score1.isEnabled = false
        edit_score2.isEnabled = false
        edit_score1.text = Editable.Factory.getInstance().newEditable(game.goalsScored.toString())
        edit_score2.text = Editable.Factory.getInstance().newEditable(game.goalsReceived.toString())
        date_picker.visibility = View.GONE
        tv_place_text.text = Editable.Factory.getInstance().newEditable(game.place)
        tv_place_text.isEnabled = false
        date_picker.setText(game.date)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (message == -1) {
            menuInflater.inflate(R.menu.save_button_menu, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.button_save_menu) {
            // verify required inputs
            if (edit_score1.text.isEmpty() || edit_score2.text.isEmpty() || tv_place_text.text.isNullOrEmpty()) {
                Toast.makeText(
                    baseContext,
                    "Score and location cannot be blank!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                saveGame()
                Toast.makeText(baseContext, "Game added succesfully.", Toast.LENGTH_SHORT).show()
                val intent = Intent(this, MainActivity::class.java)
                this.startActivity(intent)
            }
        } else {
            onBackPressed()
        }
        return true
    }

    private fun saveGame() {
        val goals = Integer.parseInt(edit_score1.text.toString())
        val received = Integer.parseInt(edit_score2.text.toString())
        val result = calcResult(goals, received)
        val team1AttackerName = team1_attacker_text_view.text.toString()
        val team1DefenderName = team1_defender_text_view.text.toString()
        val team2AttackerName = team2_attacker_text_view.text.toString()
        val team2DefenderName = team2_defender_text_view.text.toString()
        val new = Game(
            0,
            date_picker.text.toString(),
            goals,
            received,
            tv_place_text.text.toString(),
            result.value,
            team1AttackerName,
            team1DefenderName,
            team2AttackerName,
            team2DefenderName
        )
        val newGameDto = NewGameDTO(
            new, team1AttackerName, team1DefenderName,
            team2AttackerName, team2DefenderName, result
        )
        repository.addNewGame(newGameDto)
    }

    private fun calcResult(goals: Int, received: Int): GameResult {
        return when {
            goals > received -> GameResult.WIN
            received > goals -> GameResult.LOSS
            else -> GameResult.DRAW
        }
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {}

    override fun onNothingSelected(arg0: AdapterView<*>) {}

}
