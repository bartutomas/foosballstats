package com.muni.pv239.foosballstats.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.muni.pv239.foosballstats.AddGameActivity
import com.muni.pv239.foosballstats.data.repository.RecyclerRepository
import com.muni.pv239.foosballstats.databinding.FragmentHomeBinding
import com.muni.pv239.foosballstats.ui.adapter.RecyclerAdapter

class HomeFragment : Fragment() {

    private lateinit var adapter: RecyclerAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var binding: FragmentHomeBinding
    private lateinit var repository: RecyclerRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context?.let {
            repository = RecyclerRepository(it)
        }
    }

    override fun onResume() {  // After a pause OR at startup
        super.onResume()
        val itemList = repository.getAllGames().toMutableList()
        adapter = RecyclerAdapter(itemList)
        binding.homeRecycler.adapter = adapter
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)

        binding.addNew.setOnClickListener {
            val intent = Intent(context, AddGameActivity::class.java)
            context?.startActivity(intent)
        }

        linearLayoutManager = LinearLayoutManager(context)
        binding.homeRecycler.layoutManager = linearLayoutManager


        val itemList = repository.getAllGames().toMutableList()
        adapter = RecyclerAdapter(itemList)
        binding.homeRecycler.adapter = adapter
        return binding.root
    }
}
