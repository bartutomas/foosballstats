package com.muni.pv239.foosballstats.ui.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.muni.pv239.foosballstats.AddPlayerActivity
import kotlinx.android.synthetic.main.profile_item.view.*

private var context: Context? = null

class PlayersItemHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
    private var view: View = v

    init {
        v.setOnClickListener(this)
        context = v.context
    }


    fun bind(item: String) {
        view.tv_name.text = item
    }

    override fun onClick(v: View) {
        Log.d("RecyclerView", "CLICK! on"+view.tv_name.text.toString())
        val intent = Intent(context, AddPlayerActivity::class.java)
        intent.putExtra("player", view.tv_name.text)
        context?.startActivity(intent)
    }
}