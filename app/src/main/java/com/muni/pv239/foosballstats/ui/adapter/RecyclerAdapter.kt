package com.muni.pv239.foosballstats.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muni.pv239.foosballstats.R
import com.muni.pv239.foosballstats.model.Games
import com.muni.pv239.foosballstats.utils.inflate

class RecyclerAdapter(private val itemList: MutableList<Games>) :
    RecyclerView.Adapter<ItemHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val inflatedView = parent.inflate(R.layout.recycler_item, false)
        return ItemHolder(inflatedView)
    }

    override fun getItemCount(): Int = itemList.size


    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val item = itemList[position]
        holder.bind(item)
    }
}