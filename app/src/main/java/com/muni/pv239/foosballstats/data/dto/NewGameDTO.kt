package com.muni.pv239.foosballstats.data.dto

import com.muni.pv239.foosballstats.model.Game

data class NewGameDTO(
    val game: Game, val team1Attacker: String, val team1Defender: String,
    val team2Attacker: String, val team2Defender: String, val team1Result: GameResult
)